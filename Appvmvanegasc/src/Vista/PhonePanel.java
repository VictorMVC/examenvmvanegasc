package Vista;

import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 *
 * @author VictorVanegas
 */
public class PhonePanel extends javax.swing.JPanel {

    public PhonePanel() {
        initComponents();
    }

    public JComboBox<String> getCmbBoxTypes() {
        return cmbBoxTypes;
    }

    public JTextField getTxtPhoneNumber() {
        return txtPhoneNumber;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        cmbBoxTypes = new javax.swing.JComboBox<>();
        txtPhoneNumber = new javax.swing.JTextField();

        setMaximumSize(new java.awt.Dimension(225, 33));
        setLayout(new java.awt.GridBagLayout());

        cmbBoxTypes.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        cmbBoxTypes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Personal", "Domiciliar", "Laboral" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 15, 2, 13);
        add(cmbBoxTypes, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 111;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 23, 8, 21);
        add(txtPhoneNumber, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JComboBox<String> cmbBoxTypes;
    public javax.swing.JTextField txtPhoneNumber;
    // End of variables declaration//GEN-END:variables
}
