package Controlador;

import Modelo.Contact;
import Vista.MainFrame;
import Vista.PhonePanel;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author VictorVanegas
 */
public class MainFrameController {

    private static MainFrameController controller;
    private final MainFrame frame;
    private final MainFrameActionListener actionListener;

    private MainFrameController() {
        frame = new MainFrame();
        actionListener = new MainFrameActionListener();
    }

    public static MainFrameController getInstance() {
        return controller == null ? controller = new MainFrameController() : controller;
    }

    private void initController() {
        frame.getBtnAddPhoneNumber().addActionListener(actionListener);
        frame.getBtnDeletePhoneNumber().addActionListener(actionListener);
        frame.getBtnClear().addActionListener(actionListener);
        frame.getBtnOpen().addActionListener(actionListener);
        frame.getBtnSave().addActionListener(actionListener);
    }

    public void showMainFrame() {
        initController();
        frame.setVisible(true);
    }

    private class MainFrameActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "AddPhoneNumber":
                    AgregarTelefono();
                    break;
                case "DeletePhoneNumber":
                    EliminarNumero();
                    break;
                case "Save":
                    GuardarContacto();
                    break;
                case "Clear":
                    Limpiar();
                    break;
                case "Open":
                    AbrirContacto();
                    break;
                default:
                    break;
            }
        }

        private void AgregarTelefono() {
            frame.getPnlNumbers().add(new PhonePanel());
        }

        private void EliminarNumero() {
            int indexToRemove = frame.getPnlNumbers().getComponentCount() - 1;
            if (indexToRemove >= 0) {
                frame.getPnlNumbers().remove(frame.getPnlNumbers().getComponent(indexToRemove));
                frame.getPnlNumbers().repaint();
                frame.getPnlNumbers().revalidate();
            }
        }

        private void Limpiar() {
            frame.getTxtEmail().setText("");
            frame.getTxtSurname().setText("");
            frame.getTxtName().setText("");
            frame.getPnlNumbers().removeAll();
            frame.getPnlNumbers().repaint();
            frame.getPnlNumbers().revalidate();
        }

        private void GuardarContacto() {
            Contact contact = new Contact();
            contact.setName(frame.getTxtName().getText());
            contact.setLastname(frame.getTxtSurname().getText());
            contact.setEmail(frame.getTxtEmail().getText());
            contact.setPhoneNumbers(getNumeros());
            GuardarArchivo(contact);
            Limpiar();
        }

        private LinkedList<String> getNumeros() {
            LinkedList<String> phoneNumbers = new LinkedList<>();
            Component components[] = frame.getPnlNumbers().getComponents();
            PhonePanel panels[] = new PhonePanel[components.length];
            for (int i = 0; i < components.length; i++) {
                panels[i] = (PhonePanel) components[i];
            }
            String number;
            for (PhonePanel panel : panels) {
                number = String.valueOf(panel.getCmbBoxTypes().getSelectedItem()) + ","
                        + panel.getTxtPhoneNumber().getText();
                phoneNumbers.add(number);
            }
            return phoneNumbers;
        }

        private void GuardarArchivo(Contact contact) {
            File file;
            if (frame.getFileChooser().showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
                file = frame.getFileChooser().getSelectedFile();
                try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(file))) {
                    output.writeObject(contact);
                    output.flush();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        private void AbrirContacto() {
            if (frame.getFileChooser().showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                File file = frame.getFileChooser().getSelectedFile();
                try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(file))) {
                    try {
                        Contact contact = (Contact) input.readObject();
                        showContent(contact);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        private void showContent(Contact contact) {
            frame.getTxtName().setText(contact.getName());
            frame.getTxtSurname().setText(contact.getLastname());
            frame.getTxtEmail().setText(contact.getEmail());
            contact.getPhoneNumbers().stream().map(s -> {
                PhonePanel panel = new PhonePanel();
                panel.getCmbBoxTypes().setSelectedItem(s.split(",")[0]);
                panel.getTxtPhoneNumber().setText(s.split(",")[1]);
                return panel;
            }).forEachOrdered(panel -> {
                frame.getPnlNumbers().add(panel);
            });
        }
        
    }
}
